# -*- coding: utf-8 -*-



import os
import re
import sys
import yaml
from itertools import chain
from subprocess import Popen

from invoke import task
from psutil import process_iter
from sh import kdialog, posieve, posummit, svn


_filepath = os.path.join(os.path.dirname(__file__), 'config.yml')
with open(_filepath) as config_file:
    CONFIG = yaml.safe_load(config_file)


BRANCHES = (
    'branches/stable/l10n-kf5',
    'trunk/l10n-kf5',
    'trunk/l10n-kf6',
)
LANGUAGE = 'gl'
LEAF_FOLDERS = ('templates', 'scripts', LANGUAGE)
SUBVERSION_URL = 'svn+ssh://svn@svn.kde.org/home/kde'
SUPPORT_PATH = 'trunk/l10n-support'


def empty_checkout_folders():
    folders = set()
    for branch in chain(BRANCHES, (SUPPORT_PATH,)):
        parts = branch.split('/')
        for n in range(1, len(parts)+1):
            folders.add('/'.join(parts[:n]))
    return sorted(folders, key=len)


def full_checkout_folders():
    for branch in chain(BRANCHES, (SUPPORT_PATH,)):
        for leaf in LEAF_FOLDERS:
            yield os.path.join(branch, leaf)


def root_checkout_folders():
    folders = set()
    for branch in BRANCHES:
        folders.add(branch.split('/')[0])
    return folders


@task(iterable=['target', 'rule'])
def check(context, target, rule, file=None, cli=False, cache=False):
    """Checks if translation files pass quality checks

    If there are uncommited local changes, only files with changes are checked.
    Otherwise, all files are checked and a popup message is show once the check
    ends, as checking all translation files can take a long time.

    Which Pology rules are checked is configured on the ``config.yml`` file.

    Translation units that do not pass one or more quality checks are open in
    Lokalize. If there is no instance of Lokalize running, one will open
    automatically before checks start.

    You may also use the following options:

    -t, --target=TARGET
        One or more files to check, instead of all or changed.
        If you have changed files but wish to check all, use -t all.

    -r, --rule=RULE
        One or more rules to check, instead of all configured.

    -f, --file=RULE_FILE
        Custom rule file to use.

    --cli
        Do not load matches in Lokalize.

    --cache
        Use an XML cache. See the ``xml:`` option at
        http://pology.nedohodnik.net/doc/user/en_US/ch-sieve.html#sv-check-rules
    """
    if not cli and not any(process.name() == 'lokalize' for process in process_iter()):
        print('Lokalize is not running, starting an instance to show any '
              'translation issue found.')
        FNULL = open(os.devnull, 'w')
        Popen(('lokalize',), stdout=FNULL, stderr=FNULL)

    targets = []
    if target == ['all']:
        pass
    elif target:
        targets = target
    else:
        targets = svn.status('{}/{}/summit'.format(SUPPORT_PATH, LANGUAGE))
        targets = tuple(re.sub(r'^.\s+', '', target)
                        for target in targets.split('\n')
                        if target)
    if not targets:
        targets = ('{}/{}/summit'.format(SUPPORT_PATH, LANGUAGE),)
        notify = True
    else:
        notify = False

    if rule:
        rules = [
            '-s',
            'rule:{}'.format(','.join(item.decode('utf-8') for item in rule))
        ]
    elif file:
        rules = [
            '-s',
            f'rfile:{file}'
        ]
    else:
        rules = [
            '-s',
            'rule:{}'.format(','.join(CONFIG['rules']))
        ]

    targets = list(targets)
    if cache:
        targets.append('-s')
        targets.append('xml:default.xml')

    common = []
    if not cli:
        common.extend(['-s', 'lokalize'])

    posieve('--skip-obsolete',
            *common,
            '-s', 'lang:{}'.format(LANGUAGE),
            'check-rules',
            *rules,
            *targets,
            _out=sys.stdout, _err=sys.stderr)

    if notify:
        kdialog('--msgbox', 'Translation check finished')


@task(iterable=['catalog'])
def gather(context, doctype='messages', catalog=None):
    """Gather PO files from regular KDE branches into summit

    Needed for externally-injected branch catalogs, see
    https://techbase.kde.org/Localization/Workflows/PO_Summit#Externally_Injected_Branch_Catalogs

    You must specify the document type and the names of the cataglogs to
    gather:

    -d, --doctype=DOCTYPE Either ‘messages’ or ‘docmessages’.

    -c, --catalog=CATALOG One or more base file names of catalogs to gather
                          from branches.
    """
    catalog = catalog or []

    changes = svn.status('{}/{}/summit'.format(SUPPORT_PATH, LANGUAGE))
    changes = changes.strip()
    if changes:
        print('Cannot merge, found local changes:\n{}'.format(changes))
        return

    folders = tuple(full_checkout_folders())

    svn.up(*folders, _out=sys.stdout, _err=sys.stderr)

    posummit('scripts/{}.summit'.format(doctype), LANGUAGE, 'gather',
             '--create',
             '--force',
             *catalog,
             _cwd=SUPPORT_PATH,
             _out=sys.stdout, _err=sys.stderr)

    svn.revert('-R',
               '{}/{}/summit'.format(SUPPORT_PATH, LANGUAGE),
               _out=sys.stdout, _err=sys.stderr)

    svn.add('--force',  # https://stackoverflow.com/a/19131708
            *folders,
            _out=sys.stdout, _err=sys.stderr)

    svn.commit('-m', '[gl] Gather new branch catalogs into summit',
                *folders,
                _out=sys.stdout, _err=sys.stderr)

    kdialog('--msgbox', 'Gather finished')


@task
def init(context):
    """Check out the required files from the KDE Subversion repository"""
    root_folders = root_checkout_folders()
    existing_root_folders = tuple(folder for folder in root_folders
                                  if os.path.exists(folder))
    if existing_root_folders:
        print('The following folders already exist: '
              '{}'.format(', '.join(existing_root_folders)))
        return
    svn.co(SUBVERSION_URL, '.', depth='empty',
           _out=sys.stdout, _err=sys.stderr)
    svn.up(*empty_checkout_folders(), depth='empty',
           _out=sys.stdout, _err=sys.stderr)
    svn.up(*full_checkout_folders(),
           _out=sys.stdout, _err=sys.stderr)


@task
def merge(context):
    """Merge the latest summit templates with the corresponding
    translations"""
    changes = svn.status('{}/{}/summit'.format(SUPPORT_PATH, LANGUAGE))
    changes = changes.strip()
    if changes:
        print('Cannot merge, found local changes:\n{}'.format(changes))
        return

    svn.up(*full_checkout_folders(),
           _out=sys.stdout, _err=sys.stderr)

    for doctype in ('messages', 'docmessages'):
        posummit('scripts/{}.summit'.format(doctype), LANGUAGE, 'merge',
                 _cwd=SUPPORT_PATH,
                 _out=sys.stdout, _err=sys.stderr)

    svn.commit('-m', '[gl] Merge summit templates',
               '{}/{}/summit'.format(SUPPORT_PATH, LANGUAGE),
               _out=sys.stdout, _err=sys.stderr)

    kdialog('--msgbox', 'Summit merge finished')


@task
def revert(context):
    """Revert local translation changes"""
    svn.revert('-R',
               '{}/{}/summit'.format(SUPPORT_PATH, LANGUAGE),
               _out=sys.stdout, _err=sys.stderr)


@task
def scatter(context):
    """Fill all PO files from regular KDE branches with summit translations"""

    changes = svn.status('{}/{}/summit'.format(SUPPORT_PATH, LANGUAGE))
    changes = changes.strip()
    if changes:
        print('Cannot merge, found local changes:\n{}'.format(changes))
        return

    svn.up(*full_checkout_folders(),
           _out=sys.stdout, _err=sys.stderr)

    for doctype in ('messages', 'docmessages'):
        posummit('scripts/{}.summit'.format(doctype), LANGUAGE, 'scatter',
                 _cwd=SUPPORT_PATH,
                 _out=sys.stdout, _err=sys.stderr)

    folders = tuple(os.path.join(branch, LANGUAGE) for branch in BRANCHES)

    svn.add('--force',  # https://stackoverflow.com/a/19131708
            *folders,
            _out=sys.stdout, _err=sys.stderr)

    svn.commit('-m', '[gl] Scatter summit translations',
                *folders,
                _out=sys.stdout, _err=sys.stderr)

    kdialog('--msgbox', 'Summit scatter finished')


@task
def send(context, message='Progress'):
    """Send local translation changes"""
    path = '{}/{}/summit'.format(SUPPORT_PATH, LANGUAGE)
    svn.add('--force', path,
            _out=sys.stdout, _err=sys.stderr)
    svn.commit('-m', 'gl: {}'.format(message), path,
               _out=sys.stdout, _err=sys.stderr)


@task
def status(context):
    """Show the status of files within the translations folder"""
    svn.status('{}/{}/summit'.format(SUPPORT_PATH, LANGUAGE),
               _out=sys.stdout, _err=sys.stderr)


@task
def update(context):
    """Update files needed by translators"""
    folders = tuple('{}/{}/summit'.format(SUPPORT_PATH, leaf)
                    for leaf in LEAF_FOLDERS)
    svn.up(*folders, _out=sys.stdout, _err=sys.stderr)
