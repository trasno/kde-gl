================================
KDE Galician Translation Toolkit
================================

This document explains how to translate KDE_ software into Galician_.

.. _Galician: https://en.wikipedia.org/wiki/Galician_language
.. _KDE: https://kde.org/

Maintainers
===========

These are maintainers of the Galician translation of KDE software:

-   Adrián Chaves (Gallaecio) <adrian@chaves.io>

-   Miguel Branco <mgl.branco@gmail.com> (inactive)

-   Marce Villarino <mvillarino@gmail.com> (inactive)

If you plan to start contributing, you might want to email them to introduce
yourself. Also, feel free to contact them if you have any question.

If you do not get an answer, try to contact Proxecto Trasno instead, either
throught the `Proxecto Trasno mailing list`_ or through the Proxecto Trasno
Telegram group.

.. _Proxecto Trasno mailing list: http://trasno.gal/lista-de-correo-2/


Get Started
===========

#.  Find at l10n.kde.org_ a translation file that is not completely translated
    and download it.

#.  Install Lokalize_ or other translation software with Gettext support.

#.  Open the downloaded translation file with your translation software, and
    translate or modify the existing translation of one or more messages.

#.  Send an email with your translation file attached to an active maintainer.

That is all! A maintainer will let you know after reviewing and uploading your
translation file. The next release of the software that you translated will
include your work, and your name will be included in the credits.

.. _l10n.kde.org: https://l10n.kde.org/stats/gui/stable-kf5/team/gl/
.. _Lokalize: https://kde.org/applications/development/org.kde.lokalize


Avoid Duplicate Work
====================

To prevent two or more people from working on the same translation file at the
same time, consider emailing an active maintainer before you start working on
one or more files. That way, maintainers can let you know if someone is already
working on any of them, and let other people know that you are working on them
so they do not.


Make Consistent Translations
============================

Consistency, which is one of the `KDE goals`_, also applies to translations.

Benefits of consistency in translations include:

-   Increased software and documentation usability: users recognize words and
    expressions that are common across KDE applications and documentation,
    which makes everything easier to understand.

-   Better maintainability: reduced wording inconsistency simplifies making
    bulk changes to all translations when the RAG or Trasno extend or change
    their criteria.

-   Reduced complexity of translating new text: high-quality translations can
    be reused from translation memories with minimal changes, allowing to
    complete new translations much faster without sacrificing quality.

To make your translations consistent:

-   Avoid using two different words or expressions to refer to the same
    concept.

-   Follow the criteria of `Real Academia Galega`_.

-   Where RAG gives multiple choices, or none, follow the criteria of
    `Proxecto Trasno`_.

-   Where neither has a criteria, follow the criteria that has been previously
    followed in the corresponding translation file, other translation files for
    the same KDE application, other KDE software, other free software, or other
    software in general.

-   Where there is no criteria, and the right criteria is not obvious (multiple
    different translations are possible), research the pros and cons of each
    option, and expose your findings to the `Proxecto Trasno mailing list`_, to
    start a discussion that can lead to a new Proxecto Trasno criteria.

.. _KDE goals: https://kde.org/goals
.. _Proxecto Trasno: http://trasno.gal/recursos/
.. _Real Academia Galega: https://academia.gal/recursos


Download All Translation Files
==============================

Downloading files manually from l10n.kde.org_ can be inconvenient:

-   When you want to translate lots of files, downloading them manually can be
    tiresome.

-   For many files there are two different versions of the file available, one
    containing the messages from the stable version of the software and another
    one containing the messages from the latest, unreleased, development
    version of the software. You must either work only on the stable version,
    or work on two different files which have lots of common messages.

-   Browsing l10n.kde.org_ is slower than browsing a translation project in
    Lokalize or a local folder using a file browser.

All of this can be improved by downloading the translation files locally, from
a special folder (``summit``) not available at l10n.kde.org_ where different
versions of the same file are merged into a single file.

Downloading translation files requires:

-   ~100 MiB of disk space

-   `Installing Subversion`_

.. _Installing Subversion: https://subversion.apache.org/packages.html

Once you meet those requirements, open a terminal (e.g. press ``F4`` if you are
using the Dolphin file browser), write the following Subversion command and
press ``Enter``::

    svn checkout svn://anonsvn.kde.org/home/kde/trunk/l10n-support/gl/summit/messages kde-gl

A ``kde-gl`` folder is created with all translation files. You can browse the
folder contents and open any file in your translation software.

If you use Lokalize, you can create a Lokalize project inside the folder to
browse the translation files from Lokalize. (see__)

__ https://kde.org/images/screenshots/lokalize.png

If you use Dolphin as file browser, install the ``kdesdk-thumbnailers`` system
package if available, and you will be able to enable previews of Gettext
translation files. (see__)

__ https://cdn.pling.com/img//hive/content-pre1/142036-1.png

Moreover, you will be able to use Subversion in a terminal to perform some
useful actions on the downloaded translation files:

-   Check which files you have modified::

        svn status

    Helpful to locale files that you have modified accidentally, or to find
    out which files to send to a maintainer.

-   Revert your changes to one or more files::

        svn revert <file1> <file2> <…>

    After you send your changes to a maintainer, you should revert all your
    local changes, to avoid conflicts upon updating translation files later::

        svn revert -R .

-   Update the folder contents::

        svn update

If you use Dolphin as file browser, install `Dolphin Plugins`_ to be able to
perform these actions from the Dolphin context menu instead of a terminal.
(see__)

__ https://cdn.kde.org/screenshots/dolphin-plugins/dolphin-plugins.png

.. _Dolphin Plugins: https://kde.org/applications/system/org.kde.dolphin_plugins

If you install a few common command-line tools (`GNU Grep`_, `GNU Tar`_), you
can combine them with Subversion to quickly create an archive with all your
modified files that you can send to a maintainer::

    tar -caf archive.tar.xz $(svn st | grep -E '.po$' | cut -b 9-)

.. _GNU Grep: https://www.gnu.org/software/grep/
.. _GNU Tar: https://www.gnu.org/software/tar/

..
    Note: GNU Core Utils, which provides the ‘cut’ command, is not mentioned
    because it is already a dependency of Subversion:
    coreutils → util-linux → apr → apr-util → serf → subversion


Run Automated Checks
====================

Pology_ is a collection of command-line tools that allow advanced processing of
Gettext translation files.

Among other things, Pology allows to perform automated checks of rules on
Gettext translation files. Proxecto Trasno maintains a set of rules for
Galician translations which aim to detect message translations that do not
follow the criteria of RAG and Trasno.

You can run check if your translations pass these rules to find and fix any
detectable issue before sending your translations to a maintainer.

To install Pology:

#.  Install Git_.

#.  Open a terminal, preferrably _not_ within the translations folder
    (``kde-gl``), and download the Pology repository::

        git clone git://anongit.kde.org/pology.git

    This creates a ``pology`` folder.

#.  Configure the ``PATH`` environment variable::

        export PATH="<POLOGYDIR>/bin:$PATH"

    Where ``<POLOGYDIR>`` must be replaced by the path to the downloaded
    ``pology`` folder.

    Put this command in the shell startup script (``~/.bashrc`` for Bash
    shell) so that you do not need to run it again every time you open a new
    terminal.

After you install Pology, you can use its ``posieve`` command to run checks for
Galician rules. See ``pology/lang/gl/rules/README.rst`` for instructions.

.. _Git: https://git-scm.com/book/en/v2/Getting-Started-Installing-Git
.. _Pology: http://pology.nedohodnik.net/


Search and Replace
==================

Pology also allows to easily find messages across all translation files, and
even perform automated replacements in them::

    posieve find-messages --skip-obsolete -s msgstr:"<text to find>" <folder1>

For information on the search possibilities that ``find-messages`` offers, see
the `documentation of find-messages`_.

When searching for messages, there are some useful parameters that you may want
to include in your command line:

-   ``-s case`` makes your search case sensitive.

-   ``-s nflag:fuzzy`` ignores fuzzy messages.

    Fuzzy messages are message translation pending validation from a
    translator. They are often generated automatically from similar
    translations but require manual changes before they can be used.

-   ``-s nmsgctxt:"\b((EMAIL|NAME) OF TRANSLATORS|Keywords|List\s+of\s+words)\b"``
    ignores some special messages that are seldom desired among your search
    results.

-   ``-s lokalize`` opens each search result in a running instance of
    Lokalize_.

.. _documentation of find-messages: http://pology.nedohodnik.net//doc/user/en_US/ch-sieve.html#sv-find-messages


Become a Maintainer
===================

For people to be able to translate KDE software into Galician, there must be at
least one active maintainer, a person with write permissions on the KDE
Subversion repository that can upload translation files from translators.

Having more than one maintainer is important to fight `bus factor`_ and lower
the workload of existing maintainers.

If you have been contributing Galician translations for KDE software for a
while, and yours are quality translations, an existing maintainer may
eventually contact you to suggest that you become a maintainer yourself.

If you agree to become a maintainer, you will need to recreate your ``kde-gl``
folder, which will grow beyond 3 GiB, so make sure you have the required disk
space before you start.

When you are ready, follow these steps:

#.  Remove your existing ``kde-gl`` folder.

#.  Register a `GitLab account`_ and a `KDE Identity`_ account, if you do not
    have one already.

#.  Let an existing maintainer know your ID of each account. The maintainer
    will:

    -   Request to KDE sysadmins and internationalization maintainers that you
        be granted write permissions on the `KDE Subversion repository`_.

    -   Grant you write permissions on the `kde-gl Git repository`_.

#.  `Create an SSH key pair`_ if you do not have one already.

#.  Add your public key to:

    -   Your `KDE Identity`_ account (**My Account → Manage SSH Keys**).

    -   `Your GitLab account`_.

#.  Download this repository where your old ``kde-gl`` folder used to be::

        git clone git@gitlab.com:trasno/kde-gl.git

#.  Include yourself in the list of maintainers:

    #.  Edit ``kde-gl/README.rst`` to add yourself to the list of maintainers.

    #.  Open a terminal inside ``kde-gl``.

    #.  Make sure you did not change anything else accidentally::

            git status  # shows changed paths
            git diff  # shows changed content

    #.  Upload your change::

            git commit -am "Add myself as a maintainer"
            git push

#.  Install:

    -   `kdialog <https://github.com/KDE/kdialog>`_

    -   `Pology <https://invent.kde.org/sdk/pology/>`_

    -   `Python <https://www.python.org/downloads/>`_

#.  Open a terminal inside ``kde-gl`` and prepare a `Python virtual
    environment`_::

        python3 -m venv venv
        . venv/bin/activate
        pip install -r requirements.txt

    ``. venv/bin/activate`` activates the virtual environment in your terminal.
    Most commands described in this documentation require the virtual
    environment to be enabled first.

    You must remember to re-enable your environment on every new terminal
    session. Run ``deactivate`` within your virtual environment to manually
    exit the virtual environment.

#.  Wait for the maintainer to confirm that you have been granted write
    permissions on the KDE Subversion repository, which is a requirement for
    the following steps.

#.  Start an SSH session in your terminal so that your SSH credentials are not
    asked anymore for the remaining of the terminal session::

        eval `ssh-agent`
        ssh-add

#.  Download all required files::

        invoke init

Your new ``kde-gl`` folder is now ready for your new responsibilities as a
maintainer. You can continue translating, and you should, but your workflow
will be slightly different. Continue reading to prepare for your maintainer
role.

.. _bus factor: https://en.wikipedia.org/wiki/Bus_factor
.. _Create an SSH key pair: https://gitlab.com/help/ssh/README#generating-a-new-ssh-key-pair
.. _GitLab account: https://gitlab.com/users/sign_in
.. _KDE Identity: https://identity.kde.org/
.. _KDE Subversion repository: https://websvn.kde.org/
.. _kde-gl Git repository: https://gitlab.com/trasno/kde-gl
.. _Python 2 virtual environment: https://docs.python.org/2.7/glossary.html#term-virtual-environment
.. _Your GitLab account: https://gitlab.com/profile/keys


Manage the Summit Folder
========================

We are using Summit_, a workflow that allows for translators to work on a
single folder (``kde-gl/trunk/l10n-support/gl/summit/messages``) instead of
having to work and synchronize different branches.

However, Summit requires that a maintainer performs the following additional
tasks every now and then:

-   Merge the latest summit templates with the corresponding translations::

        invoke merge

-   Fill all PO files from regular KDE branches with summit translations::

        invoke scatter


.. _Summit: https://techbase.kde.org/Localization/Workflows/PO_Summit


Translate as a Maintainer
=========================

Every time you want to work on translations:

#.  Start an SSH session in your terminal so that your SSH credentials are not
    asked anymore for the remaining of the terminal session::

        eval `ssh-agent`
        ssh-add

#.  Download the latest translation files::

        invoke update

#.  Translate

    On Lokalize_, select **Project → Open Project** and select
    ``kde.lokalize``.

#.  Check that there are no unexpected files among your changes::

        invoke status

#.  Make sure that your changes pass quality checks::

        invoke check

#.  Submit your work::

        invoke send -m 'Description of my changes'

    Or just::

        invoke send

If you have local changes and you wish to revert them locally instead of
submitting them, use::

    invoke revert

You may also use regular ``svn revert`` to revert specific files listed by
``invoke status``::

    svn revert FILE
